local Vec2 = require "tools.vec2"
local Particle = require "objects.particle"
local ParticleGenerator = {}
math.randomseed(os.time())

function ParticleGenerator.create(generator_time, spawn_rate, position, lifetime, velocity, direction, dispersion, type, scale)
    local particle_generator = {}
    particle_generator.generator_time = generator_time or 10
    particle_generator.spawn_rate = spawn_rate or 0.2
    particle_generator.position = position or Vec2:new(0,0)
    particle_generator.lifetime = lifetime or 2
    particle_generator.velocity = velocity or Vec2:new(0,0)
    particle_generator.direction = direction or Vec2:new(0,0)
    particle_generator.dispersion = dispersion or Vec2:new(0,0)
    particle_generator.type = type or "RECT"

    particle_generator.elapsed_time = 0
    particle_generator.elapsed_spawn_time = 0
    particle_generator.is_running = true

    function particle_generator:load()
        self.elapsed_time = 0
        self.elapsed_spawn_time = 0
        gameloop:addGameloop(self)
    end

    function particle_generator:update(dt)
        if self.is_running then
            self.elapsed_time = self.elapsed_time + dt
            self.elapsed_spawn_time = self.elapsed_spawn_time + dt

            if self.elapsed_spawn_time >= self.spawn_rate then
                repeat
                    local x_dispersion = math.random() * (self.dispersion.x + self.dispersion.x) + -self.dispersion.x
                    local y_dispersion = math.random() * (self.dispersion.y + self.dispersion.y) + -self.dispersion.y

                    Particle.new(
                        self.position,
                        self.lifetime,
                        self.velocity,
                        Vec2:new(
                            self.direction.x + x_dispersion,
                            self.direction.y + y_dispersion
                        ),
                        self.type
                    ):load()
                    self.elapsed_spawn_time = self.elapsed_spawn_time - self.spawn_rate
                until self.elapsed_spawn_time < self.spawn_rate
            end
        end

        if self.elapsed_time >= self.generator_time then
            self:release()
        end
    end

    function particle_generator:release()
        gameloop:removeGameloop(self)
    end

    return particle_generator
end

return ParticleGenerator
