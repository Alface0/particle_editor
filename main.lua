local Renderer = require "tools/renderer"
local Gameloop = require "tools/gameloop"
local ParticleGenerator = require "particle_generator"
local Vec2 = require "tools/vec2"

renderer = Renderer:create()
gameloop = Gameloop:create()
GRAVITY = true


local PARTICLE_TYPES = {"RECT", "CIRCLE", "LINE", "POINT", "FIRE"}
local current_type = 1

local particle_generator = ParticleGenerator.create(
    100000000,
    0.02,
    Vec2:new(400, 400),
    3,
    Vec2:new(4, 4),
    Vec2:new(0, 0),
    Vec2:new(0.3, 0)
)

function love.load()
    particle_generator:load()
end

function love.draw()
    love.graphics.print("Spawn Rate (q/w): " .. particle_generator.spawn_rate, 50, 50)
    love.graphics.print("Lifetime (a/s): " .. particle_generator.lifetime, 50, 80)
    love.graphics.print("Dispersion X (e/r): " .. particle_generator.dispersion.x, 50, 110)
    love.graphics.print("Dispersion Y (t/y): " .. particle_generator.dispersion.y, 50, 140)
    love.graphics.print("Velocity X (d/f): " .. particle_generator.velocity.x, 50, 170)
    love.graphics.print("Velocity Y (g/h): " .. particle_generator.velocity.y, 50, 200)
    love.graphics.print("Direction X (x/c): " .. particle_generator.direction.x, 50, 230)
    love.graphics.print("Direction Y (v/b): " .. particle_generator.direction.y, 50, 260)
    if GRAVITY then
        love.graphics.print("Gravity (z): True", 50, 290)
    else
        love.graphics.print("Gravity (z): False", 50, 290)
    end
    love.graphics.print("Particle Type (o): " .. particle_generator.type, 50, 320)
    if particle_generator.is_running then
        love.graphics.print("Is Running (p): True", 50, 350)
    else
        love.graphics.print("Is Running (p): False", 50, 350)
    end
    renderer:draw()
end

function love.update(dt)
    gameloop:update(dt)
end

function love.keypressed(key)
    if key == "q" then
        particle_generator.spawn_rate = particle_generator.spawn_rate - 0.005
    elseif key == "w" then
        particle_generator.spawn_rate = particle_generator.spawn_rate + 0.005
    elseif key == "a" then
        particle_generator.lifetime = particle_generator.lifetime - 0.5
    elseif key == "s" then
        particle_generator.lifetime = particle_generator.lifetime + 0.5
    elseif key == "e" then
        particle_generator.dispersion.x = particle_generator.dispersion.x - 0.05
    elseif key == "r" then
        particle_generator.dispersion.x = particle_generator.dispersion.x + 0.05
    elseif key == "t" then
        particle_generator.dispersion.y = particle_generator.dispersion.y - 0.05
    elseif key == "y" then
        particle_generator.dispersion.y = particle_generator.dispersion.y + 0.05
    elseif key == "d" then
        particle_generator.velocity.x = particle_generator.velocity.x - 0.5
    elseif key == "f" then
        particle_generator.velocity.x = particle_generator.velocity.x + 0.5
    elseif key == "g" then
        particle_generator.velocity.y = particle_generator.velocity.y - 0.5
    elseif key == "h" then
        particle_generator.velocity.y = particle_generator.velocity.y + 0.5
    elseif key == "x" then
        particle_generator.direction.x = particle_generator.direction.x - 1
    elseif key == "c" then
        particle_generator.direction.x = particle_generator.direction.x + 1
    elseif key == "v" then
        particle_generator.direction.y = particle_generator.direction.y - 1
    elseif key == "b" then
        particle_generator.direction.y = particle_generator.direction.y + 1
    elseif key == "z" then
        GRAVITY = not GRAVITY
    elseif key == "o" then
        current_type = current_type + 1
        if current_type > #PARTICLE_TYPES then
            current_type = 1
        end

        particle_generator.type = PARTICLE_TYPES[current_type]
    elseif key == "p" then
        particle_generator.is_running = not particle_generator.is_running
    end
end
