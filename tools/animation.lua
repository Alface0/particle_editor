local Animation = {}

function Animation:new(images, duration)
	local animation = {}
	animation.images = images or {}
	animation.duration = duration or 0.2

	function animation:getCurrentFrame(totalTime)
		elapsedTime = totalTime % (self.duration * #self.images)
		currentFrame = math.floor((elapsedTime / (self.duration * #self.images)) * #self.images) + 1
		return self.images[currentFrame]
	end

	return animation
end

return Animation