local Renderer = {}
local number_of_layers = 5


function Renderer:create()
	local renderer = {}

	renderer.drawables = {}
	for i = 1, number_of_layers do
		renderer.drawables[i] = {}
	end

	function renderer:addRenderer(object, layer)
		local _layer = layer or 1
		table.insert(self.drawables[_layer], object)
	end

	function renderer:removeRenderer(object, layer)
		local _layer = layer or 1
		for index, value in pairs(self.drawables[_layer]) do
			if value == object then
		    	table.remove(self.drawables[_layer], index)
		    	break
			end
		end
	end

	function renderer:draw()
		for layer = 1, #self.drawables do
			for drawable = 1, #self.drawables[layer] do
				local object = self.drawables[layer][drawable]
				if object then
					object:draw()
				end
			end
		end
	end

	return renderer
end

return Renderer