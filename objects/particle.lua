local Entity = require "objects.entity"
local Animation = require "tools.animation"
local Vec2 = require "tools.vec2"
local Particle = {}

local width = 50
local height = 50

local fire_atlas = love.graphics.newImage("fire.png")
local animation_frames = {}

local frame_width = 512
local frame_height = 512

local scale = 32 / frame_width
local gravity = 0.05

for j=0,2 do
    for i=0,2 do
        table.insert(animation_frames,
            love.graphics.newQuad(
                i * frame_width,
                j * frame_height,
                frame_width,
                frame_height,
                fire_atlas:getDimensions()
            )
        )
    end
end

local animation = Animation:new(animation_frames, 0.1)

local DRAW_TYPE = {
    FIRE = function(particle)
        local currentFrame = animation:getCurrentFrame(particle.elapsed_time)
        love.graphics.draw(
            fire_atlas,
            currentFrame,
            particle.position.x,
            particle.position.y,
            0,
            scale,
            scale
        )
    end,
    RECT = function(particle)
        love.graphics.rectangle(
            "line",
            particle.position.x,
            particle.position.y,
            width * scale,
            height * scale
        )
    end,
    CIRCLE = function(particle)
        love.graphics.circle(
            "line",
            particle.position.x,
            particle.position.y,
            width * scale
        )
    end,
    LINE = function(particle)
        love.graphics.line(
            particle.position.x,
            particle.position.y,
            particle.position.x + particle.velocity.x * 3,
            particle.position.y + particle.velocity.y * 3
        )
    end,
    POINT = function(particle)
        love.graphics.points(
            particle.position.x,
            particle.position.y
        )
    end
}

function Particle.new(position, lifetime, velocity, direction, type)
    local particle = Entity:new(position.x, position.y, width, height, "Particle")
    particle.elapsed_time = 0
    particle.lifetime = lifetime
    particle.velocity = Vec2:new(velocity.x, velocity.y)
    particle.direction = direction
    particle.alpha = 1
    particle.type = type or "RECT"

    function particle:draw()
        love.graphics.setColor(255, 255, 255, self.alpha)
        DRAW_TYPE[self.type](self)
        love.graphics.setColor(255, 255, 255, 255)
    end

    function particle:update(dt)
        self.elapsed_time = self.elapsed_time + dt
        self.position.x = self.position.x + self.velocity.x * self.direction.x
        self.position.y = self.position.y + self.velocity.y * self.direction.y
        if GRAVITY then
            self.velocity.y = self.velocity.y - gravity
        end

        self.alpha = self.alpha - (1 / (self.lifetime * 60))

        if self.elapsed_time >= self.lifetime then
            self:release()
        end
    end

    return particle
end

return Particle
