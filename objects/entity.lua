local Rect = require "objects/rect"
local Vec2 = require "tools/vec2"
local Entity = {}

function Entity:new(x, y, width, height, id)
	local entity = Rect:new(x, y, width, height)

	entity.velocity = Vec2:new(0, 0)
	entity.direction = Vec2:new(0, 0)

	function entity:load()
		renderer:addRenderer(self)
		gameloop:addGameloop(self)
	end

	function entity:update(dt) end
	function entity:draw() end

	function entity:release()
		renderer:removeRenderer(self)
		gameloop:removeGameloop(self)
	end

	entity.id = id or "object"

	return entity
end

return Entity