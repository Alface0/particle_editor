# Particles Editor

This project is a simple visualizer of particle effects that allows for changing configurations in runtime.

## Examples

** Particles **

![Particles Example 1](demo_images/particles1.gif)
![Particles Example 2](demo_images/particles2.gif)
![Particles Example 3](demo_images/particles3.gif)
![Particles Example 4](demo_images/particles4.gif)

** Experimentation Video **

If you want to see some live usage and experimentation of the particles editor check the [video](https://www.youtube.com/watch?v=9y94NVTis_4).

## Dependencies

You will need to have version 11.2 of the love2D game engine.

__Archlinux__ - just install love from pacman
```sh
pacman -S love
```

__Debian__ - Since the default version in the debian repositories is the 0.9, you need to download the 11 from the [love2D](https://bitbucket.org/rude/love/downloads/) website.

__Others__ - Check your distro love version or just download from the love2d [love2D](https://bitbucket.org/rude/love/downloads/).

## Running the Editor

To run the editor go to the project folder and run the following command.
```sh
love .
```
