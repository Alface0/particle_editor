io.stdout:setvbuf("no")

function love.conf(t)
    t.version = "11.0"
    t.window.width = 800
    t.window.height = 600
    t.window.title = "Particle Generator"
end
